(ns search-a-flat.util
  "Provides html-resource method which replaces enlive method with same name using ISO-8859-1"
  (:require [net.cgrand.xml :as xml]
            [net.cgrand.enlive-html :as enlive]))

(defn- startparse-tagsoup [s ch]
  (doto (org.ccil.cowan.tagsoup.Parser.)
    (.setFeature "http://www.ccil.org/~cowan/tagsoup/features/default-attributes" false)
    (.setFeature "http://www.ccil.org/~cowan/tagsoup/features/cdata-elements" true)
    (.setFeature "http://www.ccil.org/~cowan/tagsoup/features/ignorable-whitespace" true)
    (.setContentHandler ch)
    (.setProperty "http://www.ccil.org/~cowan/tagsoup/properties/auto-detector"
      (proxy [org.ccil.cowan.tagsoup.AutoDetector] []
        (autoDetectingReader [#^java.io.InputStream is]
          (java.io.InputStreamReader. is "ISO-8859-1"))))
    (.setProperty "http://xml.org/sax/properties/lexical-handler" ch)
    (.parse s)))

(defn- load-html-resource 
 "Loads and parse an HTML resource and closes the stream."
 [stream]
  (filter map?
    (with-open [#^java.io.Closeable stream stream]
      (xml/parse (org.xml.sax.InputSource. stream) startparse-tagsoup))))

(defn html-resource 
 "Loads an HTML resource, returns a seq of nodes."
 [resource]
  (enlive/get-resource resource load-html-resource))
