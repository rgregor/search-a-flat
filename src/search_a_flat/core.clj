(ns search-a-flat.core
  (:require [net.cgrand.enlive-html :as html]
            [search-a-flat.util :as util]
            [clojure.data.xml :as xml])
  (:gen-class))

(declare offer-contents)
(declare offer-titles)
(declare fetch-resource)
(declare remove-wspaces)
(declare do-main)
(declare make-xml)
(declare insert-linebreaks)
(declare remove-date)

(defn offers-of-page [page]
  (let [base-url "http://www.flohmarkt.at/suche/mietwohnung-wien/wg-zimmer/angebote/"
        url (str base-url (if (= page 1) "" (-> page dec (* 25) str)))
        fmarkt (fetch-resource url)
        [dates contents] (offer-contents fmarkt)
        [hrefs titles] (offer-titles fmarkt)]
    (map #(do {:title %1 :href %2 :date %3 :content %4}) titles hrefs dates contents)))

(defn offer-contents [fmarkt-res]
  (let [offerings-maps (html/select fmarkt-res [:div.pMitte])
        offerings-contents (map :content offerings-maps)
        cleaned-content (map #(->> % (remove :tag) (apply str) remove-wspaces) offerings-contents)
        dates (map #(.substring % 0 19) cleaned-content)
        content-without-dates (map #(-> % remove-date remove-wspaces insert-linebreaks) cleaned-content)]
    [dates content-without-dates]))

(defn offer-titles [fmarkt-res]
  (let [title-link-maps (filter :tag (html/select fmarkt-res [:div.pMitte [(html/left [:span.biete])]]))]
    [(map #(get-in % [:attrs :href]) title-link-maps)
     (map (comp #(.toUpperCase %) remove-wspaces html/text) title-link-maps)]))
    
(defn fetch-resource
  [url]
  (util/html-resource (java.net.URL. url)))

(defn remove-date [string]
  (.substring string 19))

(defn remove-wspaces [string]
  (-> string (.replaceAll "\t" "") (.replaceAll "([ \n])[ \n]+" "$1")  .trim ))

(defn insert-linebreaks [string]
  (.replaceAll string "\n" "<br/>"))

(defn ->xml-element [{:keys [title href date content]}]
  (xml/element :item {}
               (xml/element :title {} title)
               (xml/element :description {} content)
               (xml/element :link {} href)
               (xml/element :author {} "unknown")
               (xml/element :guid {} (str title " | " date))
               (xml/element :pubDate {} date)))

(defn make-xml [items]
  (xml/element :rss {:version "2.0"}
               (xml/element :channel {}
                            (xml/element :title {} "Flohmarkt Angebote")
                            (xml/element :link {} "http://www.flohmarkt.at/suche/mietwohnung-wien/wg-zimmer/angebote")
                            (xml/element :language {} "de-de")
                            (xml/element :copyright {} "http://www.flohmarkt.at/suche/mietwohnung-wien/wg-zimmer/angebote")
                            items)))
 
(defn -main [& args]
   (let [filename (nth args 0)
         my-offers (concat (offers-of-page 1) (offers-of-page 2) (offers-of-page 3))
         my-offers-as-items (map ->xml-element my-offers)
         my-xml (make-xml my-offers-as-items)]
     (with-open [out-file (java.io.FileWriter. filename)]
       (xml/emit my-xml out-file))))
